<?php get_template_part('/inc/header'); ?>

<?php if(!get_field('disable-breadcrumbs', get_the_ID())) get_template_part('/inc/crumbs'); ?>

<?php if (have_posts()) : ?>
  <?php while (have_posts()) : the_post(); ?>
		<section class='woo-cart-wrp'>
		  <div class="container">
		    <div class="row">
					<?php the_content(); ?>
				</div>
			</div>
		</section>
  <?php endwhile; ?>
<?php endif; ?>

<?php get_template_part('/inc/footer'); ?>