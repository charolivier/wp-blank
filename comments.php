<?php if(get_template_directory() . '/comments.php' == basename($_SERVER['SCRIPT_FILENAME'])){ ?>
  <?php die ('Please do not load this page directly. Thanks!'); ?>
<?php } ?>

<?php if('open' == $post->comment_status || $comments) { ?>
  <section class='mod-comments py-5'>
    <div class='container'>
      <div class='row'>
        <div class='col-sm-12'>
          <?php if(!empty($post->post_password)) { ?>
            <?php if(!isset($_COOKIE['wp-postpass_' . COOKIEHASH]) || $_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) { ?>
              <div id="respond" class='respond'>
                <p class="nocomments">This post is password protected. Enter the password to view comments.</p>
              </div></div></div></div></section>
              <?php return; ?>
            <?php } ?>
          <?php } ?>

          <?php	$oddcomment = 'class="comments-alt" '; ?>

          <?php if('open' == $post->comment_status){ ?>
            <div id="respond" class='respond'>
              <div class='title'>
                <h2>Leave a Comment</h2>

                <?php if (get_option('comment_registration')) : ?>
                  <?php if ($user_ID) : ?>
                    <p>
                      <span>Logged in as </span>
                      <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>
                    </p>
                  <?php else : ?>
                    <p>
                      <span>You must be </span>
                      <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php the_permalink(); ?>">logged in</a>
                      <span> to post a comment.</span>
                    </p>
                  <?php endif; ?>
                <?php endif; ?>
              </div>
      
              <form class='form' action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
                <?php if (!$user_ID) : ?>
                  <input class="form-control" type="text" name="author" id="author" value="<?php echo $comment_author; ?>" placeholder="Name <?php if ($req) echo "(required)"; ?>" />
        
                  <input class="form-control" type="text" name="email" id="email" value="<?php echo $comment_author_email; ?>" placeholder="Email (will not be published) <?php if ($req) echo "(required)"; ?>" />
        
                  <input class="form-control" type="text" name="url" id="url" value="<?php echo $comment_author_url; ?>" placeholder="Website" />
                <?php endif; ?>
    
                <textarea class="form-control" name="comment" id="comment" tabindex="4" placeholder='Your comment'></textarea>
      
                <input class="btn btn-primary" name="submit" type="submit" id="submit" tabindex="5" value="Submit Comment" />
      
                <input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" />

                <?php do_action('comment_form', $post->ID); ?>
              </form>
            </div>
          <?php } ?>

          <?php if($comments){ ?>
            <div id="comments" class='comments mt-5'>
              <div class='title'>
            	  <h2 class="title-txt"><?php comments_number('No Responses', 'Responses', 'Responses' );?></h2>

              	<p class="title-nav">
              		<?php post_comments_feed_link('<span class="feedlink badge badge-primary">Feed</span>'); ?>

              		<?php if('open' == $post-> ping_status): ?>
                    <a href="<?php trackback_url() ?>" title="Copy this URI to trackback this entry.">
                      <span class="badge badge-primary">Trackback Address</span>
                    </a>
                  <?php endif; ?>
                </p>
              </div>

            	<ul class="commentlist">
              	<?php foreach ($comments as $comment) : ?>
              		<li <?php echo $oddcomment; ?>id="comment-<?php comment_ID() ?>">
                    <h5 class="heading">
                      <strong>
                  			<span><?php comment_author_link() ?></span>
                        <span> says:</span>
                      </strong>
                      <br/>
                			<?php if($comment->comment_approved == '0'): ?>
                			  <span><em>Your comment is awaiting moderation.</em></span>
                			<?php endif; ?>
                			<small><?php comment_date('F jS, Y') ?> at <?php comment_time() ?></small>
                      <small class='d-none'>(<a href="#comment-<?php comment_ID() ?>" title="">#</a>)</small>
                      <br/>
                      <small><?php edit_comment_link('<span class="badge badge-primary">Edit</span>','',''); ?></small>
                    </h5>
                    <p><?php comment_text() ?></p>
              		</li>
              	<?php $oddcomment = ( empty( $oddcomment ) ) ? 'class="comments-alt" ' : ''; ?>
              	<?php endforeach; ?>
            	</ul>
          	</div>
          <?php } ?>
        </div>
      </div>
    </div>
  </section>
<?php } ?>