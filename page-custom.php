<?php /* Template Name: Custom Template */ ?>

<?php get_template_part('/inc/header'); ?>

<?php if(!get_field('disable-breadcrumbs', get_the_ID())) get_template_part('/inc/crumbs'); ?>

<?php get_template_part('/inc/modules'); ?>

<?php get_template_part('/inc/footer'); ?>