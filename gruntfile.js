module.exports = function(grunt) {
  var proj = 'kapno-1.0';
  var excl = ['./node_modules','./.git','**/*.DS_Store','.bowerrc','.ftppass','.sass-cache'];
  var dest = '/public_html/wp-content/themes/kapno';
  var host = 'files.000webhost.com';
  var sass = require('node-sass');
  
  grunt.initConfig({
    'pkg': grunt.file.readJSON('package.json'),
 
    'watch':  {
      css: {
        files: '**/*.scss',
        tasks: ['sass:dist']
      },
      js: {
        files: [
          './public/theme/**/*.js',
          './public/admin/**/*.js'
        ],
        tasks: ['browserify','concat','uglify']
      }
    },

    'sass_globbing': {
      your_target: {
        files: {
          'public/theme/sass/_themeMap.scss' : 'public/theme/sass/theme/**/*.scss',
          'public/theme/sass/_snippetsMap.scss' : 'public/theme/sass/snippets/**/*.scss',
          'public/theme/sass/_pagesMap.scss' : 'public/theme/sass/pages/**/*.scss',
        },
        options: {
          useSingleQuotes: false
        }
      }
    },

    'sass': {
      dist: {
        options: {
          implementation: sass,
    			sourceMap: false,
          // style: 'expanded' // 'compressed'
        },
        files: {
          './public/dist/theme.css' : './public/theme/sass/style.scss',
          './public/dist/theme.adm.css' : './public/admin/global/sass/style.scss',
          './public/dist/theme.adm.edit.css' : './public/admin/edit/sass/style.scss',
          './public/dist/theme.adm.logo.css' : './public/admin/logo/sass/style.scss',
          './public/dist/theme.adm.favicon.css' : './public/admin/favicon/sass/style.scss',
        }
      }
    },

    'concat': {
      adm: {
        src: './public/admin/global/js/*.js',
        dest: './public/dist/theme.adm.js'
      },
      adm_edit: {
        src: './public/admin/edit/js/*.js',
        dest: './public/dist/theme.adm.edit.js'
      },
      adm_logo: {
        src: './public/admin/logo/js/*.js',
        dest: './public/dist/theme.adm.logo.js'
      },
      adm_favicon: {
        src: './public/admin/favicon/js/*.js',
        dest: './public/dist/theme.adm.favicon.js'
      },
    },
    
    'browserify': {
      theme: {
        files: {
          './public/dist/theme.js': './public/theme/js/theme.js'
        },
        options: {
          transform: [['babelify', { presets: "es2015" }]],
          browserifyOptions: {debug: true}
        }
      }
    },

    'uglify': {
      theme: {
        src: './public/theme/js/*.js',
        dest: './public/dist/theme.min.js'
      },
      adm: {
        src: './public/admin/global/js/*.js',
        dest: './public/dist/theme.adm.min.js'
      },
      adm_edit: {
        src: './public/admin/edit/js/*.js',
        dest: './public/dist/theme.adm.edit.min.js'
      },
      adm_logo: {
        src: './public/admin/logo/js/*.js',
        dest: './public/dist/theme.adm.logo.min.js'
      },
      adm_favicon: {
        src: './public/admin/favicon/js/*.js',
        dest: './public/dist/theme.adm.favicon.min.js'
      },
    },

    'sftp-deploy': {
      build: {
        auth: {
          host: host,
          port: 21,
          authKey: 'key'
        },
        cache: 'sftpCache.json',
        src: './',
        dest: dest,
        exclusions: excl,
        serverSep: '/',
        localSep: '/',
        concurrency: 4,
        progress: true,
      }
    },
    
    'compress': {
      main: {
        options: {
          archive: proj+'.zip'
        },
        files: [
          {src: ['*.php','*.css'], dest: '/'},
          {expand: true, cwd: 'inc/', src: ['*','components/**'], dest: 'inc'},
          {expand: true, cwd: 'public/', src: ['admin/**','dist/**','img/**'], dest: 'public'},
          {expand: true, cwd: 'settings/', src: ['*'], dest: 'settings'},
          {expand: true, cwd: 'vendors/', src: ['**'], dest: 'vendors'},
        ]
      }
    },

    'notify': {
      deploy: {
        options: {
          title: proj,
          message: 'Code deployed to' + dest
        }
      },
      zip: {
        options: {
          message: 'theme zipped'
        }
      },
    }
  });

  require('load-grunt-tasks')(grunt);

  grunt.loadNpmTasks('grunt-sass-globbing');

  grunt.loadNpmTasks('grunt-sftp-deploy');

  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.loadNpmTasks('grunt-notify');
  
  grunt.loadNpmTasks('grunt-contrib-compress');

  grunt.registerTask('default', [
    'sass:dist',

    'concat:adm',
    'concat:adm_logo',
    'concat:adm_favicon',
    'concat:adm_edit',
    'browserify:theme',

    'uglify:adm',
    'uglify:adm_logo',
    'uglify:adm_favicon',
    'uglify:adm_edit',
    'uglify:theme',

    'watch'
  ]);

  grunt.registerTask('deploy', [
    'sftp-deploy', 
    'notify:deploy'
  ]);
  
  grunt.registerTask('zip', [
    'compress',
    'notify:zip'
  ]);
};
