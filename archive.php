<?php get_template_part('/inc/header'); ?>

<?php get_template_part('/inc/crumbs'); ?>

<section class="archives-title-wrp py-5">
  <?php if (is_category()): ?>
    <h1>Posts by category &quot;<?php single_cat_title(); ?>&quot;</h1>

    <small><?php echo category_description(); ?></small>
  <?php elseif (is_author()): ?>
    <h1>Posts by author &quot;<?php the_author(); ?>&quot;</h1>
  <?php elseif (is_tag()): ?>
    <h1>Posts Tagged with &quot;<?php single_tag_title(); ?>&quot;</h1>
  <?php elseif (is_day()): ?>
    <h1>Archive for <?php the_time('F jS, Y'); ?></h1>
  <?php elseif (is_month()): ?>
    <h1>Archive for <?php the_time('F, Y'); ?></h1>
  <?php elseif (is_year()): ?>
    <h1>Archive for <?php the_time('Y'); ?></h1>
  <?php endif; ?>
</section>

<?php 
global $wp_query, $mc;

// POST
$args = array_merge($wp_query->query_vars, array('paged' => (get_query_var('paged')) ? get_query_var('paged') : 1)); 
query_posts($args);
$mc['query'] = $wp_the_query;

// SIDEBAR
$mc['side'] = array('id'=>'archive-sidebar');
$mc['side']['show'] = ($mc_theme->check_sidebar($mc['side']['id'])) ? 1 : 0;
?>

<div class="mod-archives">
  <div class="container">
    <div class="row">
      <div class="col-sm-<?php echo $mc['side']['show']?'9':'12'; ?> px-0">
        <?php if(have_posts()) get_template_part('/inc/cards');; ?>
      </div>

      <?php if($mc['side']['show']) get_template_part('/inc/side'); ?>
    </div>
  </div>
</div>

<?php $mc_theme->pagination(); ?>

<?php wp_reset_query(); ?>

<?php get_template_part('/inc/footer'); ?>
