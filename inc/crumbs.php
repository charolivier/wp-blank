<?php $mc_theme = new mc_theme(); ?>

<section class="crumbs-wrp">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <?php $mc_theme->crumbs(); ?>
      </div>
    </div>
  </div>
</section>