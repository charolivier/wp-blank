<?php $mc_theme = new mc_theme(); ?>

<?php get_template_part('/inc/head'); ?>

<?php $class=''; if (get_field('carousel-hero')) $class .= 'has-carousel-hero'; ?>
<body <?php body_class($class); ?>>

<?php get_template_part('/inc/topnav'); ?>

<main id="swup" class="transition-fade">
  <?php $class=''; if (get_field('carousel-hero')) $class .= 'has-carousel-hero'; ?>
  <noscript id="wp-theme-body-class">{
    "page":{
      "classList":"<?php echo implode(" ", get_body_class($class)) ?>",
      "id":"<?php echo get_the_ID(); ?>"
    }
  }</noscript>