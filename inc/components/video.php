<?php if(get_field("video-embed-code")) { ?>
  <section class="mod-video">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <?php the_field("video-embed-code")?>
        </div>
      </div>
    </div>
  </section>
<?php } ?>