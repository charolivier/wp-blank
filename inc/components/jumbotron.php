<section class="mod-jumbotron">
  <div class="container">
    <div class="jumbotron">
      <?php if(get_field("jumbotron-title")) { ?>
        <h1 class="display-3"><?php the_field("jumbotron-title"); ?></h1>
      <?php } ?>

      <?php if(get_field("jumbotron-subtitle")) { ?>
        <p class="lead"><?php the_field("jumbotron-subtitle"); ?></p>
      <?php } ?>

      <hr class="my-4">

      <?php if(get_field("jumbotron-paragraph")) { ?>
        <p><?php the_field("jumbotron-paragraph"); ?></p>
      <?php } ?>

      <?php if(get_field("jumbotron-button-url")) { ?>
        <p class="lead">
          <a class="btn btn-primary btn-lg" href="<?php the_field("jumbotron-button-link"); ?>" role="button">
            <?php if(get_field("jumbotron-button-text")) { ?>
              <span><?php the_field("jumbotron-button-text"); ?></span>
            <?php } else { ?>
              <span>Read More</span>
            <?php } ?>
          </a>
        </p>
      <?php } ?>
    </div>
  </div>
</section>