<?php if(get_field("map-key") && get_field("map-address")) { ?>
  <section class="mod-map" data-key="<?php the_field("map-key"); ?>">
    <div class="map-container">
      <div id="map-canvas" class="map-canvas" data-address="<?php the_field("map-address"); ?>"></div>
      <div id="info-box" class="map-tooltip"></div>
    </div>
  </section>
<?php } ?>