<?php if(get_field("mailchimp-form-action-url")) { ?>
  <section class='mod-newsletter'>
    <div class='container'>
      <div class='row'>
        <div class='col-sm-12 title-wrp mb-4'>
          <h3 class='title'>Subscribe to our news letter</h3>
          <small>Keep me posted with <em><?php echo get_bloginfo('name'); ?></em> latest news</small>
        </div>
        <div class='col-sm-12 form-wrp'>
          <form action='<?php the_field("mailchimp-form-action-url")?>' method='post' id='mc-embedded-subscribe-form' name='mc-embedded-subscribe-form' class='validate form' target='_blank' novalidate>
            <div class='form-inline pull-right'>
              <div id='mc_embed_signup'>
                <div id='mc_embed_signup_scroll'>
                  <input type='email' id='mce-EMAIL' class='form-control required email' name='EMAIL' value='' aria-required='true' placeholder='Enter email address' />

                  <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                  <div style='position: absolute; left: -5000px;'>
                    <input type='text' name='b_bc7c4ee2e6cd6c4feb3876fd1_07a2a4aa69' tabindex='-1' value=''>
                  </div>
                </div> 
              </div>

              <input type='submit' value='Subscribe' name='subscribe' id='mc-embedded-subscribe' class='btn btn-primary ml-2'>
            </div>
          </form>
        </div>
        <div class='col-sm-12 info-wrp'>          
          <div id='mce-responses' class='row'>
            <div class='response error alert alert-danger' role='alert' id='mce-error-response' style='display:none'></div>

            <div class='response success alert alert-success' role='alert' id='mce-success-response' style='display:none'></div>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php } ?>