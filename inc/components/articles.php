<?php 
global $mc;
$args = array();
if(get_field('articles-order')) {
	$args['order'] = get_field('articles-order');
}
if(get_field('articles-category')) {
  $cat = get_the_category(get_field('articles-category')); 

  if(!empty($q)) {
    $args['cat'] = $cat[0]->slug;
  }
}
if(get_field('articles-max-amount')) {
	$args['posts_per_page'] = ((int) get_field('articles-max-amount') - 1);
}
query_posts($args);
$mc['query'] = $GLOBALS['wp_query'];
if(have_posts()) {
	get_template_part('/inc/cards');
}
wp_reset_query();