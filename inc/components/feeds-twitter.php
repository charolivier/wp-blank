<?php if (get_field('twitter-screen-name')) { ?>
  <?php $mc_theme = new mc_theme(); ?>

  <section class="mod-twitter">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="title-wrp">
            <h3 class="title"><i class='fab fa-twitter'></i> @<?php echo get_field('twitter-screen-name'); ?></h3>
          </div>

          <?php $tweets = $mc_theme->get_tweets(get_field('twitter-screen-name'), get_field('tweets-amount')); ?>
          <?php if($tweets) { ?>
            <ul class='tweet-list'>
              <?php foreach ($tweets as $tweet) { ?>
                <li class='tweet-item'>
                  <i class='fa fa-quote-left'></i>
                  <div class="tweet-text">
                    <p><?php echo $mc_theme->tweet_to_HTML($tweet); ?></p>
                    <small><?php echo date('M d Y, H:i', strtotime($tweet['created_at'])); ?></small>
                  </div>
                </li>
              <?php } ?>
            </ul>
          <?php } else { ?>
            <div class='alert alert-danger' role='alert'>No tweet found</div>
          <?php } ?>
        </div>
      </div>
    </div>
  </section>
<?php } ?>