<?php if(get_field('carousel-slide')) { ?>
  <section class="mod-carousel <?php if(get_field('carousel-hero')) { ?>carousel-hero<?php } ?>">
    <div id="mc-carousel" class="carousel slide carousel-fade">
      <ol class="carousel-indicators">
      <?php $car_nav_idx=0; while(the_repeater_field('carousel-slide')) { ?>
          <li data-target="#mc-carousel" data-slide-to="<?php echo $car_nav_idx++; ?>" <?php echo ($car_nav_idx==1) ? 'class="active"' : '' ?>><span></span></li>        
        <?php } ?>
      </ol>

      <div class="carousel-inner">
        <?php $car_sli_idx=0; while(the_repeater_field('carousel-slide')) { ?>
          <div class="carousel-item <?php $car_sli_idx++; echo ($car_sli_idx==1) ? 'active' : ''?>">
						<div class="mcg-loader"></div>
						<div class="carousel-item-wrp loading">
	            <?php if (get_sub_field('image')) { ?>
	              <img class="carousel-item-img" data-src="<?php the_sub_field('image'); ?>">
	            <?php } ?>

	            <?php if (get_sub_field('title') || get_sub_field('subtitle') || get_sub_field('paragraph') || get_sub_field('button-url')) { ?>
	              <div class="carousel-caption">
	                <div class="container">
	                  <?php if (get_sub_field('title')) { ?>
	                    <div class='row title'>
	                      <div class='col-sm-12'>
	                        <h3><?php the_sub_field('title'); ?></h3>
	                      </div>
	                    </div>
	                  <?php } ?>

	                  <?php if (get_sub_field('subtitle')) { ?>
	                    <div class='row subtitle'>
	                      <div class='col-sm-12'>
	                        <h5><?php the_sub_field('subtitle'); ?></h5>
	                      </div>
	                    </div>
	                  <?php } ?>

	                  <?php if (get_sub_field('paragraph')) { ?>
	                    <div class="row paragraph">
	                      <div class='col-sm-12'>
	                        <p><?php the_sub_field('paragraph'); ?></p>
	                      </div>
	                    </div>
	                  <?php } ?>

	                  <?php if (get_sub_field('button-link')) { ?>
	                    <div class='row cta'>
	                      <div class='col-sm-12'>
	                        <a class='btn btn-primary btn-lg' href='<?php the_sub_field('button-link'); ?>'>
	                          <span><?php echo (get_sub_field('button-text')) ? the_sub_field('button-text'):'Read more'; ?></span>
	                        </a>
	                      </div>
	                    </div>
	                  <?php } ?>
	                </div>
	              </div>
	            <?php } ?>
						</div>
          </div>
        <?php } ?>
      </div>

      <a class="carousel-control-prev" href="#mc-carousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>

      <a class="carousel-control-next" href="#mc-carousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </section>
<?php } ?>