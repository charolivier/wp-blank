<?php if(get_field('contact-form-email')) { ?>
  <section class='mod-contact'>
    <div class='container'>
      <div class='row'>
        <div class='col-sm-12'>
          <div class="title-wrp">
            <h3 class="title">Send a message to the administrator</h3>

            <small>We will respond within the next 24 hours</small>
          </div>

          <form id='contact-form'>
            <div class='row'>
              <div class='col-sm-6'>
                <div class='form-group'>
                  <input name='contact_form_name' class='form-control' type='text' placeholder='Your name' />
                </div>
              </div>
              <div class='col-sm-6'>
                <div class='form-group'>
                  <input name='contact_form_email' class='form-control' type='email' placeholder='Your email address' />
                </div>
              </div>
            </div>

            <div class='form-group'>
              <input name='contact_form_subject' class='form-control' type='text' placeholder='Subject' />
            </div>

            <div class='form-group'>
              <textarea name='contact_form_message' class='form-control' placeholder='Message'></textarea>
            </div>

            <div class='form-group'>
              <div class='g-recaptcha' data-sitekey='<?php the_field('captcha-public-key'); ?>'></div>
            </div>
            
            <div class='form-cta'>
              <input name='contact_form_id' type='hidden' value='<?php echo get_the_ID(); ?>' />

              <button type='submit' class='btn btn-primary'>
                <span class='text'>send</span>
                <span class='load'></span>
              </button>
            </div>
          </form>

          <div class='alert' role='alert'></div>
        </div>
      </div>
    </div>
  </section>
<?php } ?>