<?php $posttags = get_the_tags(); ?>

<?php if ($posttags) { ?>
  <div class="tag-list-wrp">
      <ul class="tag-list">
        <?php foreach($posttags as $tag) { ?>
          <li class="tag">
            <a class="badge badge-primary" href="<?php echo get_term_link($tag->term_id); ?>">
              <span><?php echo $tag->name; ?></span>
            </a>
          </li> 
        <?php } ?>
      </ul>
  </div>
<?php } ?>