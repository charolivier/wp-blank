<?php 
global $mc;
$mc_theme = new mc_theme();
$side_class = 'mod-aside col-sm-3';
if(array_key_exists('position', $mc['side'])) $side_class .= ' side-'.$mc['side']['position']; 
?>

<div class="<?php echo $side_class; ?>">
	<div id="widget-area" class="side widget-area">
		<?php dynamic_sidebar($mc['side']['id']); ?>
	</div>
</div>