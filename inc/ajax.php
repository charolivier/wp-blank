<?php 
global $mc;

check_ajax_referer($mc['theme']['name'] . '_ajax_nonce', 'nonce');

if(isset($_POST['method']) && $_POST['method']==='test') {
	// echo 'ajax test - success';
	die();
}

if(isset($_POST['method']) && $_POST['method']==='mail') {
	// VARS
	$id=sanitize_text_field($_POST["id"]);
	$name=sanitize_text_field($_POST["name"]);
	$email=sanitize_email($_POST["email"]);
	$subject=sanitize_text_field($_POST["subject"]);
	$message=sanitize_textarea_field($_POST["message"]);
	$secret=get_field('captcha-secret-key', $id);
	$response=$_POST["captcha"];
	$verify=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secret}&response={$response}");
	$captcha_success=json_decode($verify);
	$error = array();

	// VALIDATION
	if($captcha_success->success == 0) $error[] = 'Invalid recaptcha';
	if(empty($name)) $error[] = 'Empty field: Name is required';
	if(empty($subject)) $error[] = 'Empty field: Subject is required';
	if(empty($email)) $error[] = 'Invalid email address';
	if(empty($message)) $error[] = 'Empty field: Message is required';

	// ERROR
	if(!empty($error)) {
		echo json_encode(array('success'=>0,'error'=>$error)); 
		die();
	}

	// MAIL
  $to = get_field('contact-form-email', $id);
	$msg = get_field('contact-form-success-message', $id);
  $msg = (empty($msg)) ? 'Thank you for your inquiry. Your message is on its way.' : $msg;
	$headers = 'From: ' . $email;
	$subject = 'New message from ' . home_url() . ' - ' . $subject;
	wp_mail($to, $subject, $message, $headers);
	echo json_encode(array('success'=>1,'msg'=>$msg));
	die();
}
