</main>

<footer>
  <section class="terms py-3">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <small>© <?php echo get_bloginfo("name"); ?> <?php echo date("Y"); ?> All Rights Reserved.</small>
        </div>
        <div class="col-md-6 text-right">
          <?php wp_nav_menu(array('theme_location'=>'footer-menu', 'menu_class'=>'footer-nav', 'container'=>'', 'container_class'=>'footer-nav')); ?>
        </div>
      </div>
    </div>
  </section>
</footer>

<?php get_template_part('/inc/modal'); ?>

<?php wp_footer(); ?>
</body>
</html>