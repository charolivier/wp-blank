<?php 
$mc_theme = new mc_theme();
global $mc; 
?>

<section class='single-title-wrp'>
  <h1><?php the_title(); ?></h1>

  <small>
    <span><?php echo get_the_date(); ?></span>
    <span><?php if(!empty(get_the_author())) { echo " by " . get_the_author(); } ?></span>
  </small>
</section>

<section class='single-content-wrp'>
  <div class="container">
    <div class="row">
      <?php $mc['side']['show'] = 0; if(isset($mc['side']['id']) && $mc_theme->check_sidebar($mc['side']['id'])) $mc['side']['show'] = 1; ?>

      <?php if(isset($mc['side']) && $mc['side']['show'] && $mc['side']['position']=='left') get_template_part('/inc/side'); ?>

      <div class="entry-content-wrp col-sm-<?php echo (isset($mc['side']) && $mc['side']['show'])?'9':'12'?>">
        <div class="entry-content">
          <?php the_content(); ?>

          <div class="post-terms">
            <?php get_template_part('/inc/post-tags'); ?>

            <?php get_template_part('/inc/post-cats'); ?>
          </div>
        </div>
      </div>

      <?php if(isset($mc['side']) && $mc['side']['show'] && $mc['side']['position']=='right') get_template_part('/inc/side'); ?>
    </div>
  </div>
</section>