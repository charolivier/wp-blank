<section class='share-wrp'>
  <ul class='share-list'>
    <?php $actual_link = 'http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]'; ?>
    <li>
      <a target='_blank' href='http://www.facebook.com/share.php?u=<?php echo $actual_link; ?>&title=<?php get_template_part('/inc/title'); ?>'>
        <i class='fab fa-facebook-f'></i>
      </a>
    </li>
    <li>
      <a target='_blank' href='http://twitter.com/intent/tweet?status=<?php get_template_part('/inc/title'); ?>+<?php echo $actual_link; ?>'>
        <i class='fab fa-twitter'></i>
      </a>
    </li>
    <li>
      <a target='_blank' href='http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $actual_link; ?>&title=<?php get_template_part('/inc/title'); ?>&source=<?php echo site_url(); ?>'>
        <i class='fab fa-linkedin-in'></i>
      </a>
    </li>
    <li>
      <a target='_blank' href='https://plus.google.com/share?url=<?php echo $actual_link; ?>'>
        <i class='fab fa-google'></i>
      </a>
    </li>
    <li>
      <a target='_blank' href='http://www.reddit.com/submit?url=<?php echo $actual_link; ?>&title=<?php get_template_part('/inc/title'); ?>'>
        <i class='fab fa-reddit-alien'></i>
      </a>
    </li>
    <li>
      <a target='_blank' href='http://www.tumblr.com/share?v=3&u=<?php echo $actual_link; ?>&t=<?php get_template_part('/inc/title'); ?>'>
        <i class='fab fa-tumblr'></i>
      </a>
    </li>
  </ul>
</section>