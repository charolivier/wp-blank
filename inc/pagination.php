<section class='pagination-wrp'>
  <div class='navigation'>
    <?php
      previous_post_link("%link","<span><span class='arrow'>←</span> <span class='text'>Previous <span class='hidden-xs'>post</span></span></span>");
      next_post_link("%link","<span><span class='text'>Next <span class='hidden-xs'>post</span></span> <span class='arrow'>→</span></span>");
    ?>
  </div>
</section>