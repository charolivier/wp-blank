<?php 
$mc_theme = new mc_theme();

if(get_field("custom-modules-list", get_the_ID())){
  foreach(explode(",", get_field("custom-modules-list", get_the_ID())) as $module) {
    get_template_part('/inc/components/'.str_replace("acf-acf_","",$module));
  }
}
