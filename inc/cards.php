<?php 
$mc_theme = new mc_theme(); 
global $mc;
?>

<section class="mod-cards">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <?php if(have_posts()): ?>
          <div class="card-columns cc-<?php echo $mc['query']->post_count; ?>">
            <?php while(have_posts()): the_post(); ?>
              <div class="card">
                <?php if($mc_theme->get_thb()){ ?>
                  <img class="card-img-top" src="<?php echo $mc_theme->get_thb('medium'); ?>" alt="Card image cap">
                <?php } ?>

                <div class="card-body">
                  <h4 class="card-title"><?php the_title(); ?></h4>

                  <small class="text-muted"><?php echo human_time_diff(get_the_date('U'),current_time('timestamp')).' ago'; ?><?php if(!empty(get_the_author())) { echo " by " .get_the_author(); } ?></small>

                  <p class="card-text"><?php $mc_theme->the_little_excerpt(); ?></p>
                </div>

                <div class="card-footer">
                  <a class="btn btn-primary" href="<?php the_permalink(); ?>">Read more</a>
                </div>
              </div>
            <?php endwhile; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>