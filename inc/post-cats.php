<?php $postcats = get_the_category(); ?>

<?php if ($postcats) { ?>
  <div class="cat-list-wrp">
    <ul class="cat-list">
      <?php foreach($postcats as $cat) { ?>
        <li class="cat">
          <a class="badge badge-secondary" href="<?php echo get_term_link($cat->term_id); ?>">
            <span><?php echo $cat->name; ?></span>
          </a>
        </li> 
      <?php } ?>
    </ul>
  </div>
<?php } ?>