<?php $mc_theme = new mc_theme(); ?>

<section class="topnav">
  <nav class="navbar navbar-expand-md navbar-wrp py-2">
    <div class="container">
      <a class="navbar-brand" href="<?php echo home_url(); ?>">
				<?php $logo = $mc_theme->get_logo(); if($logo['use'] && $logo['url']) { ?>
        	<img class='navbar-logo' src='<?php echo $logo['url']; ?>' <?php echo (($logo['cap'])?'alt="'.$logo['cap'].'"':''); ?>>
				<?php } else { ?>
					<span><?php bloginfo('name'); ?></span>
				<?php } ?>
      </a>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <?php
        wp_nav_menu(array(
          'theme_location' => 'header-menu', // Defined when registering the menu
          'menu_id' => 'primary-menu',
          'container' => '',
          'depth' => 3,
          'menu_class' => 'navbar-nav mr-auto',
          'walker' => new wp_bootstrap_navwalker(), // This controls the display of the Bootstrap Navbar
          'fallback_cb' => 'wp_bootstrap_navwalker::fallback', // For menu fallback
        ));
        ?>

        <div class="navbar-wrp-utilities">
          <form action="<?php echo home_url(); ?>" id="navbar-search-form" class="form-inline search-form">
            <input class="form-control" type="text" placeholder="Search" name="s">

            <button class="btn btn-primary" type="submit">
              <i class="fa fa-search" aria-hidden="true"></i>
            </button>
          </form>

          <a id="navbar-account" class="btn btn-primary" href="<?php echo home_url(); ?>/wp-admin">
            <i class="fa fa-user" aria-hidden="true"></i>
          </a>
					
					<?php if (class_exists('WooCommerce')) { ?>
						<?php $cart_count = WC()->cart->get_cart_contents_count(); ?>
							
	          <a id="navbar-cart" class="btn btn-primary" href="<?php echo home_url(); ?>/cart">
	            <i class="fa fa-shopping-cart" aria-hidden="true"></i>

							<span class='woo-cart-count <?php if($cart_count>0) { ?>show<?php } ?>'>
								<?php echo $cart_count; ?>
							</span>
	          </a>
					<?php } ?>
        </div>
      </div>
    </div>
  </nav>
</section>
