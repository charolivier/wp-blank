<?php get_template_part('/inc/header');; ?>

<section class="search-title-wrp py-5">
  <h1>Search Results</h1>
  <small>Keyword: <span><?php the_search_query(); ?></span></small>
</section>

<?php global $wp_query; ?>

<?php $args = array_merge($wp_query->query_vars, array('paged' => (get_query_var('paged')) ? get_query_var('paged') : 1)); ?>

<?php query_posts($args); ?>

<?php 
if(have_posts()){
  get_template_part('/inc/cards');;
} else { ?>
  <section class="mod-jumbotron py-5">
    <div class="container">
      <div class="jumbotron text-center">
        <p class="lead">Search returned no result. Use this form to search something else.</p>
        <div class="text-center d-inline-block">
          <form action="<?php echo home_url(); ?>" id="jumbotron-search-form" class="form-inline search-form">
            <input class="form-control" type="text" placeholder="Search" name="s">

            <button class="btn btn-primary" type="submit">
              <i class="fa fa-search" aria-hidden="true"></i>
            </button>
          </form>
        </div>
      </div>
    </div>
  </section>
<?php } ?>

<?php $mc_theme->pagination(); ?>

<?php wp_reset_query(); ?>

<?php get_template_part('/inc/footer'); ?>
