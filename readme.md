### Blank
Wordpress Theme version 0.0.1

### Install
$ cd ../path/to/theme
$ npm install

### Dev
$ grunt

### DB
#### Import wp_posts
From wp-admin/import.php, run wp importer
import theme/resource/wp/theme-unit-test-data.xml
#### Import woo products
From wp-admin/import.php, run woo importer
import theme/resource/woo/sample_products.csv

### Woo Commerce
#### Fixing the default product page
From http://localhost:8888/wp-admin/post-new.php?post_type=page
Create new page "Product"

From wp-admin/options-permalink.php
Go to Woocommerce -> Settings -> Products tab -> Display
Under the Shop & product pages heading, select your custom Shop page
Save changes
