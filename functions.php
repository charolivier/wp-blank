<?php

// CONFIG
if(!defined('ABSPATH')) exit;
define('MC_TMP_DIR', dirname(__FILE__));
define('MC_TMP_URL', esc_url(get_template_directory_uri()));

// HIDE ADMIN BAR
add_filter('show_admin_bar', '__return_false');

// THEME CLASS
require_once(MC_TMP_DIR . '/settings/class.php');
$mc_theme = new mc_theme();

// GLOBAL VARIABLES
add_action('init', array($mc_theme,'init'));

// SCRIPTS AND STYLES
add_action('wp_enqueue_scripts', array($mc_theme, 'wp_enqueue_scripts'));
add_action('admin_enqueue_scripts', array($mc_theme, 'wp_enqueue_scripts_admin'));
add_action('login_head', array($mc_theme, 'wp_enqueue_scripts_login'));
add_filter('script_loader_tag', array($mc_theme, 'add_script_attribute'), 10, 2);

// IMAGES
add_filter('the_content', array($mc_theme, 'give_linked_images_class'));
add_theme_support('post-thumbnails');

// MENU
require_once(MC_TMP_DIR . '/vendors/bootstrap-navwalker-master/bootstrap-navwalker.php');
add_action('init', array($mc_theme, 'register_my_menus'));

// HEAD
add_action('init', array($mc_theme,'removeHeadLinks'));
remove_action('wp_head', 'wp_generator');

// PAGINATION
add_filter('next_post_link', array($mc_theme, 'post_link_attributes_next'));
add_filter('previous_post_link', array($mc_theme, 'post_link_attributes_prev'));

// LOGIN LOGO HREF
add_filter('login_headerurl', array($mc_theme, 'login_header_url'));

// GALLERY
add_filter('post_gallery', array($mc_theme, 'my_post_gallery'), 10, 2);

// ACF :: Define path and URL to the ACF plugin.
// https://www.advancedcustomfields.com/
define( 'MY_ACF_PATH', get_stylesheet_directory() . '/vendors/acf/' );
define( 'MY_ACF_URL', get_stylesheet_directory_uri() . '/vendors/acf/' );
define( 'MY_ACF_REPEATER_PATH', get_stylesheet_directory() . '/vendors/acf-repeater/' );
// ACF :: Include the ACF plugin && ACF plugin repeater.
include_once( MY_ACF_PATH . 'acf.php' );
include_once( MY_ACF_PATH . 'acf-fields-group.php');
include_once( MY_ACF_REPEATER_PATH . 'acf-repeater.php');
// ACF :: Customize the url setting to fix incorrect asset URLs.
add_filter('acf/settings/url', 'my_acf_settings_url');
function my_acf_settings_url( $url ) {return MY_ACF_URL;}

// WIDGET
add_action('widgets_init', array($mc_theme, 'widgets'));
add_filter('get_search_form', array($mc_theme, 'my_search_form'), 100);

// SCREEN
add_action('current_screen', array($mc_theme, 'get_current_screen'));

// ADMIN EDIT PAGE UI
add_action("wp_ajax_get_admin_edit_ui", array($mc_theme, 'get_admin_edit_ui'));

// CUSTOM THEME TABLE
add_action('init', array($mc_theme, 'create_mc_settings_table'));

// LOGO
add_action('init', array($mc_theme, 'create_logo_table_row'));
add_action('admin_menu', array($mc_theme, 'logo_settings_menu_options'));

// FAVICON
add_action('init', array($mc_theme, 'create_favicon_table_row'));
add_action('admin_menu', array($mc_theme, 'favicon_settings_menu_options'));
add_action('admin_head', array($mc_theme, 'get_favicon'));
add_action('wp_head', array($mc_theme, 'get_favicon'));

// AJAX
add_action('wp_ajax_mc_ajax', array($mc_theme, 'mc_ajax'));
add_action('wp_ajax_nopriv_mc_ajax', array($mc_theme, 'mc_ajax'));

// WOOCOMMERCE
// WOOCOMMERCE PLUGIN NEEDED
function mytheme_add_woocommerce_support() {
  add_theme_support('woocommerce');
  add_theme_support( 'wc-product-gallery-zoom' );
  add_theme_support( 'wc-product-gallery-lightbox' );
  add_theme_support( 'wc-product-gallery-slider' );
}
add_action('after_setup_theme', 'mytheme_add_woocommerce_support');
add_filter('woocommerce_add_to_cart_fragments', array($mc_theme, 'iconic_cart_count_fragments'));

