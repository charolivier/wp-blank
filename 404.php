<?php get_template_part('/inc/header'); ?>

<section class='mod-jumbotron py-5'>
  <div class='container'>
    <div class='jumbotron'>
      <h1 class='display-3'>404 page</h1>
      <p class='lead'>The url you entered is not matching any page on this site.</p>
      <hr class='my-4'>
      <p class='lead'>
        <a class='btn primary' href='<?php echo home_url(); ?>'>Back to home page</a>
      </p>
    </div>
  </div>
</section>

<?php get_template_part('/inc/footer'); ?>