<?php /* Template Name: Index */ ?>

<?php get_template_part('/inc/header'); ?>

<?php
$mc_theme = new mc_theme();
global $wp_query, $mc;

// POSTS
$args = array_merge($wp_query->query_vars, array('paged' => (get_query_var('paged')) ? get_query_var('paged') : 1));
query_posts($args); 
$mc['query'] = $GLOBALS['wp_query'];

// SIDEBAR
$mc['side'] = array('id'=>'index-sidebar');
$mc['side']['show'] = ($mc_theme->check_sidebar($mc['side']['id'])) ? 1 : 0;
?>

<div class="index-wrp">
  <div class="container">
    <div class="row">
      <div class="col-sm-<?php echo $mc['side']['show']?'9':'12'; ?> px-0">
        <?php if(have_posts()) get_template_part('/inc/cards');; ?>
      </div>

      <?php if($mc['side']['show']) get_template_part('/inc/side'); ?>
    </div>
  </div>
</div>

<?php $mc_theme->pagination(); ?>

<?php wp_reset_query(); ?>

<?php get_template_part('/inc/footer'); ?>