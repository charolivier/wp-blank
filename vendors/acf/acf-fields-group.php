<?php

if (function_exists("acf_add_local_field_group"))
{
	acf_add_local_field_group(array (
		'key' => 'acf_articles',
		'title' => 'articles',
		'fields' => array (
			array (
				'key' => 'field_5a9d998139848',
				'label' => 'articles-order',
				'name' => 'articles-order',
				'type' => 'select',
				'choices' => array (
					'ASC' => 'ascending',
					'DESC' => 'descending',
				),
				'default_value' => 'ASC',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_5a9d99ca39849',
				'label' => 'articles-category',
				'name' => 'articles-category',
				'type' => 'taxonomy',
				'taxonomy' => 'category',
				'field_type' => 'select',
				'allow_null' => 0,
				'load_save_terms' => 0,
				'return_format' => 'key',
				'multiple' => 0,
			),
			array (
				'key' => 'field_5a9d9a133984a',
				'label' => 'articles-max-amount',
				'name' => 'articles-max-amount',
				'type' => 'number',
				'default_value' => 6,
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => '',
				'max' => '',
				'step' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-custom.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	acf_add_local_field_group(array (
		'key' => 'acf_breadcrumbs',
		'title' => 'breadcrumbs',
		'fields' => array (
			array (
				'key' => 'field_5a9e1551c39ff',
				'label' => 'disable-breadcrumbs',
				'name' => 'disable-breadcrumbs',
				'type' => 'true_false',
				'message' => '',
				'default_value' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-custom.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'side',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	acf_add_local_field_group(array (
		'key' => 'acf_carousel',
		'title' => 'carousel',
		'fields' => array (
			array (
				'key' => 'field_5a9e1412c2548',
				'label' => 'carousel-hero',
				'name' => 'carousel-hero',
				'type' => 'true_false',
				'instructions' => 'Use carousel as hero banner',
				'message' => '',
				'default_value' => 0,
			),
			array (
				'key' => 'field_5a9d9a5d353c2',
				'label' => 'carousel-slide',
				'name' => 'carousel-slide',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_5a9d9a88353c3',
						'label' => 'title',
						'name' => 'title',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_5a9d9a9f353c4',
						'label' => 'subtitle',
						'name' => 'subtitle',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_5a9d9aa6353c5',
						'label' => 'paragraph',
						'name' => 'paragraph',
						'type' => 'textarea',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'rows' => '',
						'formatting' => 'br',
					),
					array (
						'key' => 'field_5a9d9ac0353c6',
						'label' => 'button-link',
						'name' => 'button-link',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => '',
					),
					array (
						'key' => 'field_5a9d9af1353c7',
						'label' => 'button-text',
						'name' => 'button-text',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_5a9da0b46e297',
						'label' => 'image',
						'name' => 'image',
						'type' => 'image',
						'column_width' => '',
						'save_format' => 'url',
						'preview_size' => 'full',
						'library' => 'all',
					),
				),
				'row_min' => '',
				'row_limit' => '',
				'layout' => 'row',
				'button_label' => 'Add Row',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-custom.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	acf_add_local_field_group(array (
		'key' => 'acf_contact-form',
		'title' => 'contact-form',
		'fields' => array (
			array (
				'key' => 'field_5a9d9b1c4aaba',
				'label' => 'contact-form-email',
				'name' => 'contact-form-email',
				'type' => 'email',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
			),
			array (
				'key' => 'field_5a9d9b2d4aabb',
				'label' => 'captcha-public-key',
				'name' => 'captcha-public-key',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a9d9b8f4aabc',
				'label' => 'captcha-secret-key',
				'name' => 'captcha-secret-key',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-custom.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	acf_add_local_field_group(array (
		'key' => 'acf_feeds-twitter',
		'title' => 'feeds-twitter',
		'fields' => array (
			array (
				'key' => 'field_5a9dafb076a0d',
				'label' => 'Info',
				'name' => '',
				'type' => 'message',
				'message' => 'add twitter account name (without the @)',
			),
			array (
				'key' => 'field_5a9d9cd2f4b2c',
				'label' => 'twitter-screen-name',
				'name' => 'twitter-screen-name',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a9d9ce2f4b2d',
				'label' => 'tweets-amount',
				'name' => 'tweets-amount',
				'type' => 'number',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => '',
				'max' => '',
				'step' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-custom.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	acf_add_local_field_group(array (
		'key' => 'acf_jumbotron',
		'title' => 'jumbotron',
		'fields' => array (
			array (
				'key' => 'field_5a9d9d5bef063',
				'label' => 'jumbotron-title',
				'name' => 'jumbotron-title',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a9d9d6aef064',
				'label' => 'jumbotron-subtitle',
				'name' => 'jumbotron-subtitle',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a9d9d76ef065',
				'label' => 'jumbotron-paragraph',
				'name' => 'jumbotron-paragraph',
				'type' => 'textarea',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'formatting' => 'br',
			),
			array (
				'key' => 'field_5a9d9db0ef066',
				'label' => 'jumbotron-button-link',
				'name' => 'jumbotron-button-link',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a9d9dbaef067',
				'label' => 'jumbotron-button-text',
				'name' => 'jumbotron-button-text',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-custom.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	acf_add_local_field_group(array (
		'key' => 'acf_map',
		'title' => 'map',
		'fields' => array (
			array (
				'key' => 'field_5a9d9dfe98ba4',
				'label' => 'map-address',
				'name' => 'map-address',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a9d9dea98ba3',
				'label' => 'map-key',
				'name' => 'map-key',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-custom.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	acf_add_local_field_group(array (
		'key' => 'acf_modules',
		'title' => 'modules',
		'fields' => array (
			array (
				'key' => 'field_5a9d9905580a7',
				'label' => 'custom-modules-list',
				'name' => 'custom-modules-list',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-custom.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'side',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	acf_add_local_field_group(array (
		'key' => 'acf_news-letter',
		'title' => 'news-letter',
		'fields' => array (
			array (
				'key' => 'field_5a9d9e2412f3e',
				'label' => 'mailchimp-form-action-url',
				'name' => 'mailchimp-form-action-url',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-custom.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	acf_add_local_field_group(array (
		'key' => 'acf_video',
		'title' => 'video',
		'fields' => array (
			array (
				'key' => 'field_5a9d9f077c147',
				'label' => 'video-embed-code',
				'name' => 'video-embed-code',
				'type' => 'textarea',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'formatting' => 'none',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-custom.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
