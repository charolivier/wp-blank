jQuery(document).ready(function($){
  var modulesList = [], modulesField = '';

  function upd_acf_modules(){
    modulesList = []; modulesField = '';
    $('.acf-postbox.enabled').each(function(){modulesList.push($(this).attr('id'))});
    console.log('upd_acf_modules::modulesList',modulesList);
    modulesList.map(function(e,i){modulesField += e + ((modulesList.length > i+1) ? ',' : '')});
    console.log('upd_acf_modules::modulesField',modulesField);
    $('[data-name=custom-modules-list] input[type=text]').val(modulesField);
  }

  // INIT ONLOAD
  $(document).ready(function(){
    modulesField = $('[data-name=custom-modules-list] input[type=text]').val();
    console.log('modulesField', modulesField);
    modulesList = modulesField.split(',');
    console.log('modulesList', modulesList);

    $('#postbox-container-2 .acf-postbox').each(function(){
      // console.log($(this))
      $(this).children('h2').before('<button class="remove-acf-module handlediv dashicons dashicons-trash"></button>');
    });
    modulesList.map(function(e){
      $('#'+e).addClass('enabled');
    });

    console.log('php_vars.admin_ajax',php_vars.admin_ajax);

    $.ajax({
      url: php_vars.admin_ajax,
      type: 'POST',
      data: {
        action: 'get_admin_edit_ui',
        nonce: php_vars.ajax_nonce
      },
      success: function(data) {
        console.log('ajax', data);
        $('#postbox-container-2').append(data);
        $('#postbox-container-2 .acf-postbox').each(function(){
          var id = $(this).attr('id');
          var h2 = $(this).find('h2').html();
          var cl = (!$(this).hasClass('enabled'))?'button-primary':'button-secondary disabled';
          var li = '<li class="widefat button '+cl+'" data-id="'+id+'">'+h2+'</li>';
          $('#modal-add-module').children('ul').append(li);
        }); 
      }
    });

    // DISABLE SORT IN SIDE BAR
    $('#side-sortables.meta-box-sortables').sortable({disabled: true});
    $('.postbox .hndle').css('cursor', 'pointer');
    // SHOW BREADCRUMBS IN CUSTOM PAGE TMP
    $('#acf-disable-breadcrumbs').parents('.acf-postbox').addClass('show');
  });

  // ADD MODULE
  $(document).on('click','#TB_window .button-primary', function(e){
    tb_remove();
    e.preventDefault();
    var id = $(this).data('id');
    console.log(id)
    $(this).removeClass('button-primary').addClass('button-secondary disabled');
    $('#'+id).addClass('enabled');
    upd_acf_modules();
  });

  // TRASH MODULE
  $(document).on('click','.remove-acf-module', function(e){
    e.preventDefault();
    var id = $(this).parents('.acf-postbox').attr('id');
    $('#'+id).removeClass('enabled');
    $('#modal-add-module').find('li[data-id='+id+']').removeClass('button-secondary disabled').addClass('button-primary');
    upd_acf_modules();
  });

  // SORT MODULE
  $("#normal-sortables").sortable({
      update: function() {
        upd_acf_modules();
      }
  });

  // OPEN THICKBOX MODAL
  $(document).on('click',"#btn-add-module-modal",function(){
    tb_show($(this).data('title'), '#TB_inline?inlineId='+$(this).data('id'));
    $('#TB_ajaxContent').removeAttr('style');
    return false;
  });
});