jQuery(document).ready(function($){
	var media_uploader = null;

	function open_media_uploader_image() {
		media_uploader = wp.media({
			frame: "post",
			state: "insert",
			multiple: 0
		});

		media_uploader.on("insert", function(){
			var json = media_uploader.state().get("selection").first().toJSON();

			console.log(json);

      $('#logo-url').val(json.url);
      $('#logo-image').attr('src', json.url);
      $('#logo-result').show();
      $('#logo-upload-btn').hide();
		});

		media_uploader.open();

		$(".media-menu").find("a:contains('Gallery')").remove();
		$(".media-menu").find("a:contains('Playlist')").remove();
		$(".media-menu").find("a:contains('URL')").remove();
		$(".media-menu").find('.separator').remove();
	}

	$(document).on('click', '#logo-upload-btn', open_media_uploader_image);
  $(document).on('click', '#logo-delete-btn', function(e){
    e.preventDefault();
    $('#logo-result').hide();
    $('#logo-upload-btn').show();
  });
});