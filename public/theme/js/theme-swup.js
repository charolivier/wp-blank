import Swup from 'swup';
import SwupGaPlugin from '@swup/ga-plugin';
import SwupGtmPlugin from '@swup/gtm-plugin';
const ThemeSwup = {
  Init(){
    window.theme.swup = new Swup(this.Options);
  },
  Options: {
    LINK_SELECTOR: 'a[href^="'+window.location.origin+'"]:not([data-no-swup]), a[href^="/"]:not([data-no-swup]), a[href^="#"]:not([data-no-swup])',
    FORM_SELECTOR: '[data-swup-form]',
    elements: ['#swup'],
    animationSelector: '[class*="transition--"]',
    cache: false,
    scroll: false,
    animateHistoryBrowsing: false,
    debugMode: false,
    plugins: [
      new SwupGaPlugin(),
      new SwupGtmPlugin() 
    ],
    preload: false,
    support: true,
    skipPopStateHandling(event) {
      if (event.state && event.state.source === 'swup') return false;
      return false;
    }
  }
};
export default ThemeSwup;