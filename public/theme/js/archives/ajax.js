$(function(){
  var ajaxTest = false;

  if (ajaxTest) {
    $.ajax({
  		url: php_vars.admin_ajax, 
  		type: 'POST',
  		data: {
  			action: 'mc_ajax',
  			method: 'test',
  			nonce: php_vars.ajax_nonce
  		},
  		success: function(data) {
  			console.log(data);
  		}
    });
  }
});