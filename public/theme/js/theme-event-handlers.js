import $ from 'jquery';
import Carousel from './modules/carousel';
import Contact from './modules/contact';
import Gallery from './modules/gallery';
import NewsLetter from './modules/newsletter';
import Video from './modules/video';
import Map from './modules/map';
const ThemeEventHandlers = {
  GetPageData(){
    var bodyClass = document.getElementById('wp-theme-body-class');
    if (bodyClass && bodyClass.textContent) { return JSON.parse(bodyClass.textContent) }
  },
  GetBodyClass(){
    // UPDATE WP BODY CLASS
    var data = ThemeEventHandlers.GetPageData();
    document.querySelector('body').classList = data.page.classList;
  },
  GetModules(){
    window.theme.modules = {};
    // CAROUSEL
    window.theme.modules[Carousel.name] = [];
    $(".mod-carousel").each(function(){
      const i = new Carousel({vm:$(this)});
      window.theme.modules[Carousel.name].push(i);
      i.init();
    });
    // CONTACT
    window.theme.modules[Contact.name] = [];
    $(".mod-contact").each(function(){
      const i = new Contact({vm:$(this)});
      window.theme.modules[Contact.name].push(i);
      i.init();
    });
    // NEWSLETTER
    window.theme.modules[NewsLetter.name] = [];
    $(".mod-newsletter").each(function(){
      const i = new NewsLetter({vm:$(this)});
      window.theme.modules[NewsLetter.name].push(i);
      i.init();
    });
    // GALLERY
    window.theme.modules[Gallery.name] = [];
    $(".mod-gallery").each(function(){
      const i = new Gallery({vm:$(this)});
      window.theme.modules[Gallery.name].push(i);
      i.init();
    });
    // VIDEO
    window.theme.modules[Video.name] = [];
    $("iframe").each(function(){
      const players = /www.youtube.com|player.vimeo.com/;
      const src = $(this).attr('src');
      if (src && src.search(players) !== -1) {
        const i = new Video({vm:$(this)});
        window.theme.modules[Video.name].push(i);
        i.init();
      }
    });
    // MAP
    window.theme.modules[Map.name] = [];
    $(".mod-map").each(function(){
      const i = new Map({vm:$(this)});
      window.theme.modules[Map.name].push(i);
      i.init();
    });
  },
  isWooCommerce(){
    const data = ThemeEventHandlers.GetPageData();
    if (data.page.classList && data.page.classList.indexOf('woocommerce-page') !== -1) return true;
  },
  SwupContentReplaced(){
    ThemeEventHandlers.GetBodyClass();
    ThemeEventHandlers.GetModules();
    ThemeEventHandlers.ParseLinks();
  },
  ParseLinks(){
    $('a').each(function(e){
      var href = $(this).attr('href');
      var arr = ["/shop","/cart","/checkout"];
      if (arr.some(str=>href.includes(str))) $(this).attr("data-no-swup",true)
    })
  }
};
export default ThemeEventHandlers;