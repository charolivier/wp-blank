import ThemeEventHandlers from './theme-event-handlers';
const ThemeEventListeners = {
  ContentReplaced(){
    document.addEventListener('swup:contentReplaced', ThemeEventHandlers.SwupContentReplaced);
  },
  Init(){
    ThemeEventListeners.ContentReplaced();
  }
};
export default ThemeEventListeners;