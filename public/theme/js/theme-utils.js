const Utils = {
  loadScript(src,id){
    return new Promise((resolve, reject) => {
        const script = document.createElement('script');
        if (document.getElementById(id)) {
          resolve();
        } else {
          script.async = true;
          script.src = src;
          script.id = id;
          script.addEventListener('load', resolve);
          script.addEventListener('error', () => reject('Error loading script.'));
          script.addEventListener('abort', () => reject('Script loading aborted.'));
          document.querySelector("body").insertBefore(script, document.getElementById('mcthemejs'));
        }
    });
  }
}
export default Utils;