import $ from 'jquery';
import Utils from '../theme-utils';
const gmap_api = "https://maps.googleapis.com/maps/api/js";
const gmap_info = php_vars.blog_tmp+"/vendors/gmap/map.infobox.js";
const gmap_markers = php_vars.blog_tmp+"/vendors/font-awesome-markers/fontawesome-markers.min.js";
const Map = class {
  constructor(data){
    this.name = 'Map';
    this.vm = data.vm;
  }
  async init(){
    const key = this.vm.data("key");
    if (!key) {console.log("gmap::missing key"); return}
    await Utils.loadScript(gmap_api+'?key='+key,'gmap_api');
    await Utils.loadScript(gmap_info,'gmap_info');
    await Utils.loadScript(gmap_markers,'gmap_markers');
    this.map_post();
  }
  map_post(){
    var map_canvas = $("#map-canvas");
    var map_address = map_canvas.data("address");
    if (typeof map_address !== 'undefined' && map_address.length) {
      google.maps.event.addDomListener(window, 'load', this.map_get(map_address));
      google.maps.event.addDomListener(window, 'resize', this.map_get(map_address));
    };
  }
  map_get(map_address){
    var geocoder = new (google.maps.Geocoder);
    geocoder.geocode({"address": map_address}, (results, status)=>{
      if (status == google.maps.GeocoderStatus.OK) {
        var styles_array = [
          {
            "featureType": "administrative",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#444444"
              }
            ]
          },
          {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [
              {
                "color": "#f2f2f2"
              }
            ]
          },
          {
            "featureType": "poi",
            "elementType": "all",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "road",
            "elementType": "all",
            "stylers": [
              {
                "saturation": -100
              },
              {
                "lightness": 45
              }
            ]
          },
          {
            "featureType": "road.highway",
            "elementType": "all",
            "stylers": [
              {
                "visibility": "simplified"
              }
            ]
          },
          {
            "featureType": "road.arterial",
            "elementType": "labels.icon",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "water",
            "elementType": "all",
            "stylers": [
              {
                "color": "#419bf9"
              },
              {
                "visibility": "on"
              }
            ]
          }
        ];
        var lat = results[0].geometry.location.lat();
        var lng = results[0].geometry.location.lng();
        var map_options = {
          center: {
            lat: lat,
            lng: lng
          },
          zoom: 12,
          styles: styles_array,
          scrollwheel: false,
        };
        var map = new google.maps.Map(document.getElementById('map-canvas'), map_options);
        // MARKER
        var marker = new google.maps.Marker({
          map: map,
          icon: {
            path: fontawesome.markers.MAP_MARKER,
            scale: 1,
            strokeWeight: 0,
            strokeColor: 'black',
            strokeOpacity: 1,
            fillColor: '#333',
            fillOpacity: 1,
          },
          position: map_options.center,
          url: "http://maps.google.com/maps?z=12&t=m&q=loc:"+lat+"+"+lng
        });
        // INFOBOX SETUP
        var infobox = new InfoBox({
          content: "<div class='info-box-container' style='display:none'>"+map_address+"</div>",
          disableAutoPan: false,
          alignBottom: true,
          pixelOffset: new google.maps.Size(18, -155),
          zIndex: null,
          closeBoxURL: "",
          infoBoxClearance: new google.maps.Size(1, 1)
        });
        infobox.open(map, marker);
        // EVENT
        google.maps.event.addListener(marker, 'click', ()=>{
          window.open(this.url, '_blank');
        });
      };
    });
  }
};
export default Map;