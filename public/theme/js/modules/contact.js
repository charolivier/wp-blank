import $ from 'jquery';
const Contact = class {
  constructor(data){
    this.name = 'Contact';
    this.vm = data.vm;
    this.form = this.vm.find("#contact-form");
    this.button = this.form.find('button[type="submit"]');
    this.message = this.vm.find('.alert');
  }
  init(){
    // console.log('Contact::init', this.vm);
    this.eventListener();
  }
  eventListener(){
    this.form.on("submit", e => {
      e.preventDefault();

      // UPD MESSAGE
      this.message.hide().empty().removeClass('alert-success alert-danger');

      // UPD BUTTON
      this.button.prop('disabled', 1);
      this.button.find('.text').addClass('hidden');
      this.button.find('.load').show();

      var req = {}, 
      fields = this.form.serializeArray();

      $.each(fields, function(i, field){
        req[field.name] = field.value;
      });

      $.ajax({
        type: "POST",
        url: php_vars.admin_ajax,
        data: {
          action: 'mc_ajax',
          method: 'mail',
          id: req.contact_form_id,
          name: req.contact_form_name,
          email: req.contact_form_email,
          subject: req.contact_form_subject,
          message: req.contact_form_message,
          nonce: php_vars.ajax_nonce,
          captcha: grecaptcha.getResponse()
        },
        success: data => {
          if (data) {
            var res = JSON.parse(data);
          } else {
            var res = {'success':0, 'error':['An error happend with the server.']};
          }

          if (res) {
            if (res.success) {
              this.message.addClass('alert-success').html('<p>'+res.msg+'</p>');

              this.form.remove();
            } else {
              var err = '';

              $.each(res.error, (i,el) => {
                err+='<li>'+el+'</li>';
              });

              this.message.addClass('alert-danger').html('<p>Your email was not sent.</p>').append('<ul>'+err+'</ul>');

              this.button.prop('disabled', 0);
              this.button.find('.text').removeClass('hidden');
              this.button.find('.load').hide();
            }

            this.message.show();
          }
        }
      })
    });
  }
};
export default Contact;