import $ from 'jquery';
import Fitvids from 'fitvids';
// VIDEOS
const Video = class {
  constructor(data){
    this.name = 'Video';
    this.vm = data.vm;
  }
  init(){
    this.vm.wrap('<div class="video-container"></div>');
    const $video = this.vm.closest('.video-container');
    Fitvids($video);
  }
};
export default Video;