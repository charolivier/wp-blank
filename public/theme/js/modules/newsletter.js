import $ from 'jquery';
import Utils from '../theme-utils';
const mcvalidatejs = 'https://s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js';
const NewsLetter = class {
  constructor(data){
    this.name = 'NewsLetter';
    this.vm = data.vm;
  }
  init(){
    Utils
    .loadScript(mcvalidatejs,'mcvalidate')
    .then(()=>{
      window.fnames = new Array();
      window.ftypes = new Array();
      fnames[0]='EMAIL';
      ftypes[0]='email';
      fnames[1]='FNAME';
      ftypes[1]='text';
      fnames[2]='LNAME';
      ftypes[2]='text';
      var $mcj = $.noConflict(true);
    })
    .catch(error=>{console.log(error)})
  }
}
export default NewsLetter;