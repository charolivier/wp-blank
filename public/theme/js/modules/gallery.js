import $ from 'jquery';
const Gallery = class {
  constructor(data){
    this.name = 'Gallery';
    this.vm = data.vm;
    this.gallery = null;
    this.focus = null;
    this.modal = $('#mc-modal');
    this.slideshow = null;
    this.image = null;
    this.delay = 2000;
    this.data = null;
    this.play = null;
  }
  eventListener(){
    $(document).on('click', '.mc-popup-btn', (e)=>{
      e.preventDefault();
      this.focus = $(this);
  		this.gallery = this.focus.parents('.mc-gallery');
      this.slideshow = this.modal.find('.mcg-slideshow');
      data = {
        image: this.focus.data('img'),
        caption: this.focus.data('caption'),
        title: this.focus.data('title')
      };
  		this.focus.addClass('active').siblings().removeClass('active');
  		this.slide_show();
    });
    $(document).on('click', '#mc-modal .prev', ()=>{
      this.slide_auto(false);
      this.slide_prev();
    });
    $(document).on('click', '#mc-modal .next', ()=>{
      this.slide_auto(false);
      this.slide_next();
    });
    $(document).on('click', '#mc-modal .exit', ()=>{
      this.slide_auto(false);
      this.lightbox_hide();
    });
    $(document).on('click', '#mc-modal .play', ()=>{
      this.slide_auto(true);
    });
    $(document).on('click', '#mc-modal .stop', ()=>{
      this.slide_auto(false);
    });
    $(document).on('hide.bs.modal','#mc-modal', (e)=>{
  		this.slide_auto(false);	
    });
    $(document).on('dragstart', '#mc-modal img', (e)=>{
      e.preventDefault();
    });
  }
	init() {
    // console.log('Gallery::init', this.vm);
    var $output, direction;
    // MODAL
    $output  = '<div class="mcg-slideshow"></div>';
    $output += '<div class="prev ico left transparent"><i class="fas fa-chevron-left prev"></i></div>';
    $output += '<div class="next ico right transparent"><i class="fas fa-chevron-right"></i></div>';
		this.modal.find('.modal-body').html($output);
    // CAPTION
    $output = '<div class="caption"></div>';
    this.modal.find('.modal-footer').html($output);
	  this.modal.hammer().bind("panright panleft", (ev)=>{
	    direction = ev.type;
	  });
	  this.modal.hammer().bind("panend", (ev)=>{
	    if ($(window).width() < 768) {
	      if (direction == "panright" || direction == "panleft") {
          if (direction == "panright") {
						this.slide_auto(0);
            this.slide_prev();
          };

          if (direction == "panleft") {
						this.slide_auto(0);
            this.slide_next();
          };
	      };
	    };
	  });
    // EVENT
    this.eventListener();
	}
	loader(foo) {
		if (foo) {
		  this.modal.find('.modal-body').append("<div class='mcg-loader'></div>");
		} else {
		  this.modal.find('.mcg-loader').remove();
		} 
	}
	slide_show() {
		this.loader(true);
    $('<img class="mcg-image" src="'+data.image+'">').on('load', ()=>{
			this.slideshow
      .find('.mcg-image')
			.css('opacity', 0)
			.remove();

			$(this)
			.appendTo($mc_slideshow)
			.css('opacity', 0)
			.animate({'opacity':1}, {duration:500, easing:'easeInOutExpo'})
			.promise()
			.done(()=>{
				this.loader(0);
        $('#mc-modal').modal('handleUpdate');
			});
    });
    this.modal.find('.modal-title').html(data.title);
    this.modal.find('.modal-footer .caption').html(data.caption);
	}
  slide_next() {
    if (this.gallery) {
      if (this.gallery.find('li:last-child').hasClass('active')){
        this.focus = this.gallery.find('li:first-child');
        } else {
        this.focus = this.focus.next('li');
      }

      this.focus.addClass('active').siblings().removeClass('active');

      data = {
        image: this.focus.data('img'),
        caption: this.focus.data('caption'),
        title: this.focus.data('title')
      };

      this.slide_show();
    }
  }
  slide_prev() {
    if (this.gallery) {
      if (this.gallery.find('li:first-child').hasClass('active')){
        this.focus = this.gallery.find('li:last-child');
        } else {
        this.focus = this.focus.prev('li');
      }
      this.focus.addClass('active').siblings().removeClass('active');
      
      data = {
        image: this.focus.data('img'),
        caption: this.focus.data('caption'),
        title: this.focus.data('title')
      };

      this.slide_show();
    }
  }
  slide_auto(foo) {
    if (foo) {
      this.play = setInterval(()=>{
        this.slide_next();
      }, this.delay);
      this.modal.find('.play').addClass('active');
      this.modal.find('.stop').removeClass('active');
      } else {
      clearInterval(this.play);
      this.modal.find('.stop').addClass('active');
      this.modal.find('.play').removeClass('active');
    }
  }
};
export default Gallery;





























