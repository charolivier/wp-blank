import $ from 'jquery';
const Carousel = class {
  constructor(data){
    this.name = 'Carousel';
    this.vm = data.vm;
  }
  init(){
    // console.log('Carousel::init', this.vm);
    this.lazyLoadImg();
    this.smoothResize();
  }
  lazyLoadImg(){
    [].forEach.call(this.vm.find('.carousel-item-img'), img => {
      img.setAttribute('src', img.getAttribute('data-src'));
      img.onload = () => {
        img.removeAttribute('data-src');
        var par = img.parentNode;
        par.classList.toggle("loading");
      };
    });
  }
  smoothResize(){
    if (!this.vm.hasClass("carousel-hero")) {
      this.vm
      .find('.carousel')
      .carousel({interval: 5000})
      .on('slide.bs.carousel', (e)=>{
        var nextH = $(e.relatedTarget).height();
        $(this).find('.carousel-item.active').parent().animate({height: nextH}, 400);
      });
    }
  }
};
export default Carousel;