window.theme = window.theme || {};
window.theme.modules = window.theme.modules || {};
window.theme.swup = window.theme.swup || {};

import ThemeSwup from './theme-swup';
import ThemeEventListeners from './theme-event-listeners';
import ThemeEventHandlers from './theme-event-handlers';

function Init() {
  if (!ThemeEventHandlers.isWooCommerce()) ThemeSwup.Init();
  ThemeEventListeners.Init();
  ThemeEventHandlers.GetModules();
  ThemeEventHandlers.ParseLinks();
  console.log('window.theme::', window.theme);
  window.removeEventListener("DOMContentLoaded", Init, true);
}
window.addEventListener('DOMContentLoaded', Init, true);

