<!-- ADMIN favicon -->
<div class='wrap'>
	<h3>fav settings</h3>

	<form method='post' id='fav-settings'>
		<div class='row'>
			<input type='checkbox' id='fav-use' name='fav_use' <?php echo $this->is_checked($res['use']); ?> />

			<label for='fav-use'><span>Enable favicon</span></label>

			<br>

			<small>This feature allows you to upload your own favicon and display it in your website.</small>
		</div>
		
		<div class='row'>
			<a id='fav-upload-btn' <?php echo (($res['url']) ? "style='display:none'" : ""); ?>>
				<span class='dashicons dashicons-upload fav-upload-ico'></span>

				<span class='fav-upload-txt'>Click to upload favicon</strong>
			</a>

		  <div id='fav-result' <?php echo ((!$res['url']) ? "style='display:none'" : ""); ?>>
		    <div id='fav-img-wrp'>
					<image id='fav-image' src='<?php echo (($res['url']) ? $res['url'] : ""); ?>' />
				</div>

				<button id='fav-delete-btn' class='btn btn-primary'>Delete</button>
		  </div>
		</div>

		<div  class='row'>
		  <input type='hidden' id='fav-url' name='fav_url' value='<?php echo $res['url']; ?>' />

		  <input type='submit' id='fav-save-btn' class='btn btn-primary' value='Save Settings' name='update' />
		</div>
	</form>
</div>