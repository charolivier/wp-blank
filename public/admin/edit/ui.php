<?php 
global $mc;

check_ajax_referer($mc['theme']['name'] . '_ajax_nonce', 'nonce');

echo '<a class="button-primary" href="#" id="btn-add-module-modal" data-title="Add custom modules to page" data-id="modal-add-module"><span>Add Module</span></a><div id="modal-add-module" style="display:none"><ul class="clearfix"></ul></div>';
?>