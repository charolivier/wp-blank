jQuery(document).ready(function($){
  // FORCE FORM TO SUBMIT ON TEMPLATE UPDATE
  $('#page_template').change(function(e){
    e.preventDefault();

    $('#post').submit();
  });
});