<!--ADMIN LOGO -->
<div class='wrap'>
	<h3>Logo settings</h3>

	<form method='post' id='logo-settings'>
		<div class='row'>
			<input type='checkbox' id='logo-use' name='logo_use' <?php echo $this->is_checked($res['use']); ?> />

			<label for='logo-use'><span>Enable logo</span></label>

			<br>

			<small>This feature allows you to upload your own logo and display it in your website.</small>
		</div>
		
		<div class='row'>
			<a id='logo-upload-btn' <?php echo (($res['url']) ? "style='display:none'" : ""); ?>>
				<span class='dashicons dashicons-upload logo-upload-ico'></span>

				<span class='logo-upload-txt'>Click to upload logo</strong>
			</a>

		  <div id='logo-result' <?php echo ((!$res['url']) ? "style='display:none'" : ""); ?>>
		    <div id='logo-img-wrp'>
					<image id='logo-image' src='<?php echo (($res['url']) ? $res['url'] : ""); ?>' />
				</div>

				<div id='logo-cap-wrp'>
					<input type='text' name='logo_cap' value='<?php echo (($res['cap']) ? $res['cap'] : ''); ?>' placeholder='caption' />
				</div>

				<button id='logo-delete-btn' class='btn btn-primary'>Delete</button>
		  </div>
		</div>

		<div  class='row'>
		  <input type='hidden' id='logo-url' name='logo_url' value='<?php echo $res['url']; ?>' />

		  <input type='submit' id='logo-save-btn' class='btn btn-primary' value='Save Settings' name='update' />
		</div>
	</form>
</div>