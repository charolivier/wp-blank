<?php get_template_part('/inc/header'); ?>

<?php if(!get_field('disable-breadcrumbs', get_the_ID())) get_template_part('/inc/crumbs'); ?>

<section class='woo-wrp'>
  <div class="container">
    <div class="row">
			<div class='woo-content col-sm-12'>
				<?php woocommerce_content(); ?>
			</div>
		</div>
	</div>
</section>

<?php get_template_part('/inc/footer'); ?>