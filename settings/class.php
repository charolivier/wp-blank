<?php
class mc_theme {
  var $screen;
  var $google_api_key;
  var $mc_table;
  var $db;
  // CONSTRUCT
  function __construct() {
    global $wpdb, $mc;
    $this->db = $wpdb;
    $this->mc_table = $this->db->prefix . "mc";
  }
  // INIT
  function init() {
    global $mc;

    $mc = array();
    $mc['theme'] = array();
    $mc['theme']['version'] = 1.0;
    $mc['theme']['name'] = 'Blank';

    $this->php_vars = array(
      'admin_ajax' => admin_url('admin-ajax.php'),
      'ajax_nonce' => wp_create_nonce($mc['theme']['name'].'_ajax_nonce'),
      'blog_tmp' => get_bloginfo('stylesheet_directory'),
      'blog_url' => home_url()
    );
  }
  // SESSION
  function start_session() {
    if(!session_id()) session_start();
  }
  function end_session() {
    session_destroy ();
  }
  // AJAX
  function mc_ajax() {
    require_once(get_template_directory() . '/inc/ajax.php');
    //die();
  }
  // HELPERS
  function is_checked($value) {
    if ($value == 1) {
      return 'checked="checked"';
    }
  }
  function is_selected($selected,$value) {
    if ($selected == $value) {
      return 'selected="selected"';
    }
  }
  function alternate($index) {
    if ($index % 2 == 0) {
      return 'alternate';
    }
  }
  function has_image($value) {
    if (strlen($value)) {
      return "has_image";
    }
  }
  // WP SCRIPTS AND STYLES
  function wp_enqueue_scripts() {
    if(!is_admin()) {
      $dependencies = array('jquery');
      wp_deregister_script('jquery');
      wp_enqueue_style('font-awesome', MC_TMP_URL . '/vendors/fontawesome-free-5.10.1-web/css/all.min.css');
      wp_enqueue_style('bootstrap-css-nav', MC_TMP_URL . '/vendors/bootstrap-nav/css/bootstrap-4-navbar.css');
      wp_enqueue_style('mc-theme-css', MC_TMP_URL . '/public/dist/theme.css', '', time());
      wp_register_script('jquery', MC_TMP_URL . '/vendors/jquery/dist/jquery.min.js', 0, 0, 1); 
      wp_enqueue_script('jquery');
      wp_enqueue_script('jquery-ui', MC_TMP_URL . '/vendors/jquery-ui/jquery-ui.min.js', $dependencies, 0, 1);
      wp_enqueue_script('popper-js', MC_TMP_URL . '/vendors/popper/popper.min.js', $dependencies, 0, 1);
      wp_enqueue_script('bootstrap-js', MC_TMP_URL . '/vendors/bootstrap/dist/js/bootstrap.min.js', $dependencies, 0, 1);
      wp_enqueue_script('bootstrap-nav-js', MC_TMP_URL.'/vendors/bootstrap-nav/js/bootstrap-4-navbar.js', $dependencies, 0, 1);
      wp_enqueue_script('hammer-js', MC_TMP_URL.'/vendors/hammer/hammer.min.js', $dependencies, 0, 1);
      wp_enqueue_script('hammer-jquery', MC_TMP_URL.'/vendors/hammer/jquery.hammer.js', $dependencies, 0, 1);
      wp_enqueue_script('recaptcha','https://www.google.com/recaptcha/api.js', $dependencies, 0, 1);
      wp_enqueue_script('mcthemejs', MC_TMP_URL.'/public/dist/theme.js', $dependencies, time(), 1);
      wp_localize_script('mcthemejs', 'php_vars', $this->php_vars);
    }
  }
  function wp_enqueue_scripts_admin() {
    wp_enqueue_style('mc-theme-adm-css', MC_TMP_URL.'/public/dist/theme.adm.css');
    wp_enqueue_script('mc-theme-adm-js', MC_TMP_URL.'/public/dist/theme.adm.js', array('jquery'));
    wp_localize_script('mc-theme-adm-js', 'php_vars', $this->php_vars);

    if($this->is_edit_page()) {
      if($this->check_page_template('page-custom.php') || $this->check_page_template('index.php')) {
        wp_enqueue_script('thickbox');
        wp_enqueue_style('thickbox');
        wp_enqueue_style('mc-theme-adm-edit-css', MC_TMP_URL.'/public/dist/theme.adm.edit.css');
        wp_enqueue_script('mc-theme-adm-edit-js', MC_TMP_URL.'/public/dist/theme.adm.edit.js', array('jquery'));
      }
    }

    if($_GET && isset($_GET['page']) && $_GET['page']=='logo_settings') {
      wp_enqueue_media();
      wp_enqueue_style('logo_css', MC_TMP_URL.'/public/dist/theme.adm.logo.css');
      wp_enqueue_script('logo_script', MC_TMP_URL.'/public/dist/theme.adm.logo.min.js', array('jquery'));
    }

    if($_GET && isset($_GET['page']) && $_GET['page']=='favicon_settings') {
      wp_enqueue_media();
      wp_enqueue_style('favicon_css', MC_TMP_URL.'/public/dist/theme.adm.favicon.css');
      wp_enqueue_script('favicon_script', MC_TMP_URL.'/public/dist/theme.adm.favicon.min.js', array('jquery'));
    }
  }
  function wp_enqueue_scripts_login() {
    wp_enqueue_style('bootstrap-css', MC_TMP_URL.'/vendors/bootstrap/css/bootstrap.css');
    wp_enqueue_style('mc-theme-css', MC_TMP_URL.'/public/dist/theme.css');
  }
  function add_script_attribute($tag, $handle) {
    if ($handle === 'recaptcha') return str_replace(' src', ' async defer src', $tag);
    if ($handle === 'mcthemejs') return str_replace(' src', ' id="mcthemejs" src', $tag);
    return $tag;
  }
  // IMAGES
  function catch_that_image() {
    global $post, $posts;
    $first_img = "";
    ob_start();
    ob_end_clean();
    $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
    if($output>0) {
      $first_img = $matches[1][0];
    } else {
      return 0;
      // $first_img = MC_TMP_URL.'/public/img/image-not-found.png';
    }
    return $first_img;
  }
  function get_thb($size='large') {
    if (has_post_thumbnail()):
      return wp_get_attachment_url(get_post_thumbnail_id(get_the_id()), $size);
    else:
      return $this->catch_that_image();
    endif;
  }
  function give_linked_images_class($content) {
    $attr = 'class="attachment" data-popup="image_detail" ';

    $content = preg_replace('/(<a.*?)><img/', '$1 '.$attr.'><img', $content);

    return $content;
  }
  // REGISTER MENUS
  function register_my_menus() {
    register_nav_menus(array(
      "header-menu" => __( "header-menu" ),
      "footer-menu" => __( "footer-menu" )
      )
    );
  }
  // HEAD
  function removeHeadLinks() {
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'wp_print_styles', 'print_emoji_styles' ); 
  }
  // PAGINATION
  function pagination() {
    global $wp_query, $wp_rewrite;
    $pages = '';
    $max = $wp_query->max_num_pages;
    if (!$current = get_query_var('paged')) $current = 1;
    $a['base'] = str_replace(999999999, '%#%', esc_url(get_pagenum_link(999999999)) );
    $a['total'] = $max;
    $a['current'] = $current;
    $total = 1; //1 - display the text "Page N of N", 0 - not display
    $a['mid_size'] = 5; //how many links to show on the left and right of the current
    $a['end_size'] = 1; //how many links to show in the beginning and end
    //text of the "Previous page" link
    $a['prev_text'] = '<span class="prev">←</span>'; 
    //text of the "Next page" link
    $a['next_text'] = '<span class="next">→</span>';
    if ($max > 1) {
      echo "<div class='pagination-wrp py-4 text-center'><div class='navigation'>";
      echo $pages . paginate_links($a);
      echo "</div></div>";
    } 
  }
  function post_link_attributes_next($output) {
    $code = 'class="btn btn-primary next ml-1"';
    return str_replace('<a href=', '<a '.$code.' href=', $output);
  }
  function post_link_attributes_prev($output) {
    $code = 'class="btn btn-primary prev mr-1"';
    return str_replace('<a href=', '<a '.$code.' href=', $output);
  }
  // FAVICON
  function favicon() {
    $blogurl = MC_TMP_URL;
    $favurl = '/assets/img/fav.png';
    echo "<link href='".$blogurl."/".$favurl."?v=2'  rel='shortcut icon' />";
  }
  // DATE
  function date($from, $to='') {
    if ( empty($to) ) {
      $to = time();
    }
    $diff = (int) abs( $to - $from );
    if ( $diff <= HOUR_IN_SECONDS ) {
      $mins = round( $diff / MINUTE_IN_SECONDS );
      if ( $mins <= 1 ) {
        $mins = 1;
      }
      $since = sprintf( _n( '%s min', '%s mins', $mins ), $mins );
    } elseif ( ( $diff <= DAY_IN_SECONDS ) && ( $diff > HOUR_IN_SECONDS ) ) {
      $hours = round( $diff / HOUR_IN_SECONDS );
      if ( $hours <= 1 ) {
        $hours = 1;
      }
      $since = sprintf( _n( '%s hour', '%s hours', $hours ), $hours );
    } elseif ( $diff >= DAY_IN_SECONDS ) {
      $days = round( $diff / DAY_IN_SECONDS );
      if ( $days <= 1 ) {
        $days = 1;
      }
      $since = sprintf( _n( '%s day', '%s days', $days ), $days );
    }
    return $since;
  }
  // BREADCRUMBS
  function crumbs() {
    $showOnHome = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
    $delimiter = '&raquo;'; // delimiter between crumbs
    $home = 'Home'; // text for the 'Home' link
    $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
    $before = '<span class="current">'; // tag before the current crumb
    $after = '</span>'; // tag after the current crumb
 
    global $post;

    $homeLink = home_url();
 
    if (is_home() || is_front_page()) {
      if ($showOnHome == 1) echo '<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a></div>';
    } else {
      echo '<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';
 
      if ( is_category() ) 
      {
        $thisCat = get_category(get_query_var('cat'), false);
        if ($thisCat->parent != 0) echo get_category_parents($thisCat->parent, TRUE, ' ' . $delimiter . ' ');
        echo $before . 'Archive by category "' . single_cat_title('', false) . '"' . $after;
 
      } 
      elseif ( is_search() ) 
      {
        echo $before . 'Search results for "' . get_search_query() . '"' . $after;
 
      } 
      elseif ( is_day() ) 
      {
        echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
        echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
        echo $before . get_the_time('d') . $after;
      } 
      elseif ( is_month() ) 
      {
        echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
        echo $before . get_the_time('F') . $after;
      } 
      elseif ( is_year() ) 
      {
        echo $before . get_the_time('Y') . $after;
      } 
      elseif ( is_single() && !is_attachment() ) 
      {
        if ( get_post_type() != 'post' ) 
        {
          $post_type = get_post_type_object(get_post_type());
          $slug = $post_type->rewrite;
          echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a>';
          if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
        } 
        else 
        {
          $cat = get_the_category(); $cat = $cat[0];
          $cats = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
          if ($showCurrent == 0) $cats = preg_replace("#^(.+)\s$delimiter\s$#", "$1", $cats);
          echo $cats;
          if ($showCurrent == 1) echo $before . get_the_title() . $after;
        }
 
      } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
        $post_type = get_post_type_object(get_post_type());
        echo $before . $post_type->labels->singular_name . $after;
 
      } elseif ( is_attachment() ) {
        $parent = get_post($post->post_parent);
        $cat = get_the_category($parent->ID); $cat = $cat[0];
        echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
        echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a>';
        if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
 
      } elseif ( is_page() && !$post->post_parent ) {
        if ($showCurrent == 1) echo $before . get_the_title() . $after;
 
      } elseif ( is_page() && $post->post_parent ) {
        $parent_id  = $post->post_parent;
        $breadcrumbs = array();
        while ($parent_id) {
          $page = get_page($parent_id);
          $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
          $parent_id  = $page->post_parent;
        }
        $breadcrumbs = array_reverse($breadcrumbs);
        for ($i = 0; $i < count($breadcrumbs); $i++) {
          echo $breadcrumbs[$i];
          if ($i != count($breadcrumbs)-1) echo ' ' . $delimiter . ' ';
        }
        if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
      } elseif ( is_tag() ) {
        echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;
 
      } elseif ( is_author() ) {
        global $author;
        $userdata = get_userdata($author);
        echo $before . 'Articles posted by ' . $userdata->display_name . $after;
      } elseif ( is_404() ) {
        echo $before . 'Error 404' . $after;
      }
 
      if ( get_query_var('paged') ) {
        if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
        echo __('Page') . ' ' . get_query_var('paged');
        if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
      }
 
      echo '</div>';
    }
  }
  // EXCERPT
  function string_limit_words($string, $word_limit) {
    $words = explode(' ', $string, ($word_limit + 1));

    if (count($words) > $word_limit):
      array_pop($words);
      //add a ... at last article when more than limit word count
      echo implode(' ', $words)."...";
    else:
      //otherwise
      echo implode(' ', $words);
    endif;
  }
  function the_little_excerpt($count=15) {
    echo $this->string_limit_words(get_the_excerpt(), $count);
  }
  // LOGIN
  function login_header_url() {
    return home_url();
  }
  // GALLERY
  function my_post_gallery($output, $attr) {
    global $post;

    if(isset($attr['orderby'])) {
      $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);

      if(!$attr['orderby']) {
        unset($attr['orderby']);
      }
    }
    
    $col = 3;

    if(isset($attr['columns'])) $col = $attr['columns'];

    extract(shortcode_atts(array(
      'order' => 'ASC',
      'orderby' => 'menu_order ID',
      'id' => $post->ID,
      'itemtag' => 'dl',
      'icontag' => 'dt',
      'captiontag' => 'dd',
      'columns' => 3,
      'size' => 'thumbnail',
      'include' => '',
      'exclude' => ''
    ), $attr));

    $id = intval($id);

    if ('RAND' == $order) {
      $orderby = 'none';
    }

    if (!empty($include)) {
      $include = preg_replace('/[^0-9,]+/', '', $include);
      $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

      $attachments = array();
      foreach ($_attachments as $key => $val) {
        $attachments[$val->ID] = $_attachments[$key];
      }
    }

    if (empty($attachments)) {
      return '';
    }

    // Here's your actual output, you may customize it to your need
    $output = "<div id='".$id."' class='mc-gallery clearfix'>";
    $output .= "<ul class='mc-gallery-list mc-gallery-col-".$col."'>";

    // Now you loop through each attachment
    foreach ($attachments as $id => $attachment) {
      $img = wp_get_attachment_image_src($id, 'full');
      $thb = wp_get_attachment_image_src($id, 'thumbnail');
      $imgTitle = $attachment->post_title;
      $imgCaption = $attachment->post_excerpt;
      $output .= "<li class='mc-popup-btn' data-img='".$img[0]."' data-title='$imgTitle' data-caption='$imgCaption'>";
      $output .= "<a id='".$id."' class='thb' style='background-image:url(".$thb[0].")' data-toggle='modal' data-target='#mc-modal'></a>";
      $output .= "</li>";
    }

    $output .= "</ul>";
    $output .= "</div>";

    return $output;
  }
  // TWITTER
  // https://apps.twitter.com
  // https://developer.twitter.com/en/docs/tweets/timelines/api-reference/get-statuses-user_timeline
  function get_tweets($screenName, $count=4) {
    $url = 'https://calm-ocean-40021.herokuapp.com?search='.$screenName.'&count='.$count;
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
    $data = curl_exec($ch);
    curl_close($ch);
    return json_decode($data, true);
  }
  function tweet_to_HTML($tweet) {
    $text = $tweet['text'];
    // hastags
    $linkified = array();
    foreach ($tweet['entities']['hashtags'] as $hashtag) {
        $hash = $hashtag['text'];

        if (in_array($hash, $linkified)) {
            continue; // do not process same hash twice or more
        }
        $linkified[] = $hash;

        // replace single words only, so looking for #Google we wont linkify >#Google<Reader
        $text = preg_replace('/#\b' . $hash . '\b/', sprintf('<a href="https://twitter.com/search?q=%%23%2$s&src=hash">#%1$s</a>', $hash, urlencode($hash)), $text);
    }
    // user_mentions
    $linkified = array();
    foreach ($tweet['entities']['user_mentions'] as $userMention) {
        $name = $userMention['name'];
        $screenName = $userMention['screen_name'];

        if (in_array($screenName, $linkified)) {
            continue; // do not process same user mention twice or more
        }
        $linkified[] = $screenName;

        // replace single words only, so looking for @John we wont linkify >@John<Snow
        $text = preg_replace('/@\b' . $screenName . '\b/', sprintf('<a href="https://www.twitter.com/%1$s" title="%2$s">@%1$s</a>', $screenName, $name), $text);
    }
    // urls
    $linkified = array();
    foreach ($tweet['entities']['urls'] as $url) {
        $url = $url['url'];

        if (in_array($url, $linkified)) {
            continue; // do not process same url twice or more
        }
        $linkified[] = $url;

        $text = str_replace($url, sprintf('<a href="%1$s">%1$s</a>', $url), $text);
    }

    return $text;
  }
  // WIDGET
  function widgets() {
    register_sidebar(array(
      'name'          => 'Index Sidebar',
      'id'            => 'index-sidebar',
      'before_widget' => '<div class="filter">',
      'after_widget'  => '</div>',
      'before_title'  => '<h3 class="title">',
      'after_title'   => '</h3>',
    ));

    register_sidebar(array(
      'name'          => 'Post Sidebar',
      'id'            => 'post-sidebar',
      'before_widget' => '<div class="filter">',
      'after_widget'  => '</div>',
      'before_title'  => '<h3 class="title">',
      'after_title'   => '</h3>',
    ));

    register_sidebar(array(
      'name'          => 'Page Sidebar',
      'id'            => 'page-sidebar',
      'before_widget' => '<div class="filter">',
      'after_widget'  => '</div>',
      'before_title'  => '<h3 class="title">',
      'after_title'   => '</h3>',
    ));

    register_sidebar(array(
      'name'          => 'Archive Sidebar',
      'id'            => 'archive-sidebar',
      'before_widget' => '<div class="filter">',
      'after_widget'  => '</div>',
      'before_title'  => '<h3 class="title">',
      'after_title'   => '</h3>',
    ));
  }
  function my_search_form($form) {
      $form = '<form action="'.home_url().'" id="widget-search-form" class="form-inline">
      <input class="form-control" type="text" placeholder="Search" name="s">
      <button class="btn btn-primary" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
      </form>';

      return $form;
  }
  function check_sidebar($id) {
    $sidebars = wp_get_sidebars_widgets();

    if(empty($sidebars[$id])) {
      return 0;
    } else {
      return 1;
    }
  }
  // SCREEN
  function get_current_screen() {
    global $current_screen;
    $this->screen = $current_screen;
  }
  // CUSTOM PAGE MODULES
  function is_edit_page($new_edit=null) {
    global $pagenow;
    //make sure we are on the backend
    if (!is_admin()) return false;
    
    if($new_edit == "edit") {
      return in_array($pagenow, array('post.php'));
    }
    //check for new post page
    elseif($new_edit == "new") {
      return in_array($pagenow, array('post-new.php'));
    }
    //check for either new or edit
    else {
      return in_array($pagenow, array('post.php', 'post-new.php'));
    }
  }
  function check_page_template($template) {
    global $post;

    $tmp = explode('/',get_post_meta($post->ID, '_wp_page_template', true));

    if(is_array($tmp)) $tmp = array_pop($tmp);

    if ($template == $tmp) {
      return true;
    } else {
      return false;
    }
  }
  // EDIT PAGE
  function get_admin_edit_ui() {
    require_once(get_template_directory() . '/public/admin/edit/ui.php');

    die();
  }
  // ACF
  function acf_settings_path($path) {
    // update path
    $path = MC_TMP_URL . '/vendors/acf/';
    // return
    return $path;
  }
  function acf_settings_dir($dir) {
    // update path
    $dir = MC_TMP_URL . '/vendors/acf/';
    // return
    return $dir;
  }
  // ACF FIELD GROUP
  function has_field_group($id, $str) {
    $modules_array = explode(",", get_field("custom-modules-list", $id));
    if (in_array("acf-acf_".$str, $modules_array)) return 1;
  }
  // THEME CUSTOM SETTINGS
  function create_mc_settings_table() {
    // Create new table if table doesn't already exist
    if ($this->db->get_var("SHOW TABLES LIKE '$this->mc_table'") != $this->mc_table) {
      $sql = "CREATE TABLE $this->mc_table (
        mc_id mediumint(9) NOT NULL AUTO_INCREMENT,
        mc_key mediumtext NOT NULL,
        mc_val mediumtext NOT NULL,
        UNIQUE KEY id (mc_id)
      )";

      require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

      dbDelta($sql);
    }
  }
  // THEME CUSTOM SETTINGS - LOGO
  function create_logo_table_row() {
    // Create new row if row doesn't already exist
    if(!$this->db->get_row("SELECT * FROM $this->mc_table WHERE mc_key='logo'")) {
      $arr=array('use'=>0,'url'=>0,'cap'=>0);

      $this->db->insert($this->mc_table, array('mc_key'=>'logo','mc_val'=>serialize($arr)));
    }
  }
  function logo_settings_menu_options() {
    add_submenu_page('themes.php', 'Logo settings', 'Logo', 'manage_options', 'logo_settings', array($this, 'logo_settings_page_options'));
  }
  function logo_settings_page_options() {
    // Dies if current is not an administrator
    if (!current_user_can('manage_options')) {
      wp_die(__('You do not have sufficient permissions to access this page.'));
    }

    // POST
    if(isset($_POST['update'])) {
      $res = array(
        'use'=>(isset($_POST['logo_use'])) ? 1 : 0,
        'url'=>($_POST['logo_url']) ? esc_url_raw($_POST['logo_url']) : 0,
        'cap'=>($_POST['logo_cap']) ? sanitize_text_field($_POST['logo_cap']) : 0,
      );

      $this->db->update($this->mc_table, array('mc_val'=>serialize($res)), array('mc_key'=>'logo'));
    }

    // GET
    $res = $this->db->get_row("SELECT * FROM $this->mc_table WHERE mc_key='logo'");
    $res = ($res) ? unserialize($res->mc_val) : 0;

    $filePath = MC_TMP_DIR.'/public/admin/logo/ui.php';

    if(is_file($filePath)) {
      ob_start();

      include($filePath);

      return ob_get_contents();

      ob_end_clean();
    }
  }
  function get_logo() {
    $res = $this->db->get_row("SELECT * FROM $this->mc_table WHERE mc_key='logo'");
    $res = ($res) ? unserialize($res->mc_val) : 0;
    return $res;
  }
  // THEME CUSTOM SETTINGS - FAVICON
  function create_favicon_table_row() {
    // Create new row if row doesn't already exist
    if(!$this->db->get_row("SELECT * FROM $this->mc_table WHERE mc_key='favicon'")) {
      $arr=array('use'=>0,'url'=>0);

      $this->db->insert($this->mc_table, array('mc_key'=>'favicon','mc_val'=>serialize($arr)));
    }
  }
  function favicon_settings_menu_options() {
    add_submenu_page('themes.php', 'Favicon settings', 'Favicon', 'manage_options', 'favicon_settings', array($this, 'favicon_settings_page_options'));
  }
  function favicon_settings_page_options() {
    // Dies if current is not an administrator
    if (!current_user_can('manage_options')) {
      wp_die(__('You do not have sufficient permissions to access this page.'));
    }

    // POST
    if(isset($_POST['update'])) {
      $res = array(
        'use'=>(isset($_POST['fav_use'])) ? 1 : 0,
        'url'=>($_POST['fav_url']) ? esc_url_raw($_POST['fav_url']) : 0,
      );

      $this->db->update($this->mc_table, array('mc_val'=>serialize($res)), array('mc_key'=>'favicon'));
    }

    // GET
    $res = $this->db->get_row("SELECT * FROM $this->mc_table WHERE mc_key='favicon'");
    $res = ($res) ? unserialize($res->mc_val) : 0;

    $filePath = MC_TMP_DIR.'/public/admin/favicon/ui.php';

    if(is_file($filePath)) {
      ob_start();

      include($filePath);

      return ob_get_contents();

      ob_end_clean();
    }
  }
  function get_favicon() {
    $res = $this->db->get_row("SELECT * FROM $this->mc_table WHERE mc_key='favicon'");
    $res = ($res) ? unserialize($res->mc_val) : 0;

    if($res && $res['use'] && $res['url']) {
      echo '<link href="'.$res['url'].'" rel="icon" type="image/x-icon">';
    }
  }
  // WOOCOMMERCE
  function iconic_cart_count_fragments($fragments) {
    $count = WC()->cart->get_cart_contents_count();

    $fragments['.woo-cart-count'] = ($count>0) ? '<span class="woo-cart-count show">'.$count.'</span>' : '<span class="woo-cart-count">'.$count.'</span>';
  
    return $fragments;
  }
}
