<?php get_template_part('/inc/header'); ?>

<?php get_template_part('/inc/crumbs'); ?>

<?php if (have_posts()) : ?>
  <?php while (have_posts()) : the_post(); ?>
		<?php 
		// SIDEBAR
		global $mc;
		$mc['side'] = array('id'=>'post-sidebar', 'position'=>'right'); 
		?>

    <?php get_template_part('/inc/post-entry'); ?>

    <?php get_template_part('/inc/share'); ?>

    <?php comments_template(); ?>

    <?php get_template_part('/inc/pagination'); ?>
  <?php endwhile; ?>
<?php endif; ?>

<?php get_template_part('/inc/footer'); ?>