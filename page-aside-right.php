<?php /* Template Name: Side Bar - Right */ ?>

<?php get_template_part('/inc/header'); ?>

<?php if(!get_field('disable-breadcrumbs', get_the_ID())) get_template_part('/inc/crumbs'); ?>

<?php if (have_posts()) : ?>
  <?php while (have_posts()) : the_post(); ?>
    <?php 
		// SIDEBAR
		global $mc;
		$mc['side'] = array('id'=>'page-sidebar');
		$mc['side']['position'] = 'right';
		?>

    <?php get_template_part('/inc/post-entry'); ?>

    <?php get_template_part('/inc/share'); ?>

    <?php comments_template(); ?>
  <?php endwhile; ?>
<?php endif; ?>

<?php get_template_part('/inc/footer'); ?>